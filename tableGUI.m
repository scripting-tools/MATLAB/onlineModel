function varargout = tableGUI(varargin)
% TABLEGUI MATLAB code for tableGUI.fig
%      TABLEGUI, by itself, creates a new TABLEGUI or raises the existing
%      singleton*.
%
%      H = TABLEGUI returns the handle to a new TABLEGUI or the handle to
%      the existing singleton*.
%
%      TABLEGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in TABLEGUI.M with the given input arguments.
%
%      TABLEGUI('Property','Value',...) creates a new TABLEGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before tableGUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to tableGUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help tableGUI

% Last Modified by GUIDE v2.5 24-Apr-2014 19:40:30

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @tableGUI_OpeningFcn, ...
                   'gui_OutputFcn',  @tableGUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before tableGUI is made visible.
function tableGUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to tableGUI (see VARARGIN)

%%%%%%%%%%%% BEGIN my code
handles = guidata(hObject);
% initialize table
if ~isempty(varargin) && isa(varargin{1}, 'onlineModel')
    handles.myModelObject=varargin{1};
else
    error('Error! No onlineModel object provided when opening gui!')
end
%

% set table rows
set(handles.dataTable, 'RowName', [...
    handles.myModelObject.otherCurrents, ...
    handles.myModelObject.quadrupoleCurrents, ...
    handles.myModelObject.correctorsCurrents, ...
    ]);
% set table values
auxValues =  [...
    handles.myModelObject.machineModel.getModelParameters(handles.myModelObject.otherCurrents);...
    handles.myModelObject.machineModel.getModelParameters(handles.myModelObject.quadrupoleCurrents);...
    handles.myModelObject.machineModel.getModelParameters(handles.myModelObject.correctorsCurrents)];
set(handles.dataTable, 'Data', auxValues);


% Choose default command line output for tableGUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes tableGUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = tableGUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in submitButton.
function submitButton_Callback(hObject, eventdata, handles)
% hObject    handle to submitButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% get new data from the table
auxValues =  get(handles.dataTable, 'Data');
%auxValues(isnan(auxValues)) = 0;
% set it to the model
handles.myModelObject.machineModel.setModelParameters(...
    [handles.myModelObject.otherCurrents, ...
    handles.myModelObject.quadrupoleCurrents, ...
    handles.myModelObject.correctorsCurrents], ...
    auxValues);

% fire an update
handles.myModelObject.updateOptics();

% --- Executes on button press in cancelButton.
function cancelButton_Callback(hObject, eventdata, handles)
% hObject    handle to cancelButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

close(handles.output);
