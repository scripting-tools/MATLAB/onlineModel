function varargout = onlineModelGUI(varargin)
% ONLINEMODELGUI MATLAB code for onlineModelGUI.fig
%      ONLINEMODELGUI, by itself, creates a new ONLINEMODELGUI or raises the existing
%      singleton*.
%
%      H = ONLINEMODELGUI returns the handle to a new ONLINEMODELGUI or the handle to
%      the existing singleton*.
%
%      ONLINEMODELGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in ONLINEMODELGUI.M with the given input arguments.
%
%      ONLINEMODELGUI('Property','Value',...) creates a new ONLINEMODELGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before onlineModelGUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to onlineModelGUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help onlineModelGUI

% Last Modified by GUIDE v2.5 12-Aug-2014 17:34:28

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @onlineModelGUI_OpeningFcn, ...
    'gui_OutputFcn',  @onlineModelGUI_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before onlineModelGUI is made visible.
function onlineModelGUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to onlineModelGUI (see VARARGIN)

%%%%%%%%%%%% BEGIN my code
handles = guidata(hObject);

if nargin < 2 || ~isa(varargin{1}, 'onlineModel')
    error('Error! start as onlineModelGUI("onlineModel object", "list of measurements positions")!')
end

handles.myModelObject=varargin{1};
handles.myListOFMeasurementPoints=varargin{2};
if numel(varargin) > 2 && ischar(varargin{3}) && strcmp(varargin{3},'PTCtwiss')
    handles.usePTCtwiss = true;
else
    handles.usePTCtwiss = false;
end

% fire an update optics
handles.myModelObject.updateOptics();

% create uimenu for optic axes
try
    handles.axesMenu = uicontextmenu();
    handles.axesMenuItemName = uimenu(handles.axesMenu, 'Label','Unknown element');
    handles.axesMenuItemBetx = uimenu(handles.axesMenu, 'Label','Betx = Unknown');
    handles.axesMenuItemBety = uimenu(handles.axesMenu, 'Label','Bety = Unknown');
    handles.axesMenuItemAlfx = uimenu(handles.axesMenu, 'Label','Alfx = Unknown');
    handles.axesMenuItemAlfy = uimenu(handles.axesMenu, 'Label','Alfy = Unknown');
    handles.axesMenuItemDx = uimenu(handles.axesMenu, 'Label','Dx = Unknown');
    handles.axesMenuItemDy = uimenu(handles.axesMenu, 'Label','Dy = Unknown');
    handles.axesMenuItemMux = uimenu(handles.axesMenu, 'Label','Mux = Unknown');
    handles.axesMenuItemMuy = uimenu(handles.axesMenu, 'Label','Muy = Unknown');
    handles.axesMenuItemX = uimenu(handles.axesMenu, 'Label','x = Unknown');
    handles.axesMenuItemY = uimenu(handles.axesMenu, 'Label','y = Unknown');
    set(handles.opticAxes,'uicontextmenu',handles.axesMenu);
    set(handles.elementsAxes,'uicontextmenu',handles.axesMenu);
catch e
    error(['Impossible to define context menu for my axes :',e.message]);
end

% add some event listeners
handles.myListenerComputed=addlistener(handles.myModelObject,'updatedOptic',@(h,e)opticComputationDone(handles));
handles.myListenerComputing=addlistener(handles.myModelObject,'computingOptic',@(h,e)opticComputationStarted(handles));
handles.myListenerCompatationError=addlistener(handles.myModelObject,'errorComputingOptic',@(h,e)opticComputationError(handles));
guidata(hObject, handles);

% populate plots
handles=populateGUI(handles);

% run an update
%handles.myModelObject.updateOptics();
%%%%%%%%%%%% END my code


% Choose default command line output for onlineModelGUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes onlineModelGUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- function used to create and populate plots
function handles = populateGUI(handles)
% populate GUI elements that needs an initialization

% Let start with controls
auxString = 'Line start';
for i=1:length(handles.myListOFMeasurementPoints)
    auxString = [auxString,'|',handles.myListOFMeasurementPoints{i}];
end
set(handles.popupLocations,'String',auxString);

set(handles.editBetx0,'String',num2str(handles.myModelObject.initialBETX));
set(handles.editBety0,'String',num2str(handles.myModelObject.initialBETY));
set(handles.editAlfx0,'String',num2str(handles.myModelObject.initialALFX));
set(handles.editAlfy0,'String',num2str(handles.myModelObject.initialALFY));
set(handles.editDeltaP,'String',num2str(handles.myModelObject.initialDELTAP));

set(handles.autoUpdateButton,'Value',...
    handles.myModelObject.isTimerRunning());
set(handles.editUpdatePeriod,'String',...
    num2str(handles.myModelObject.getTimerPeriod()));

% now with plots
hold(handles.opticAxes, 'on');
%
if handles.usePTCtwiss
    handles.betxPlot=modelLinkedPlotReadable(handles.opticAxes,...
        handles.myModelObject,...
        'S',...
        'BETA11',...
        'updatedOptic');
    handles.betyPlot=modelLinkedPlotReadable(handles.opticAxes,...
        handles.myModelObject,...
        'S',...
        'BETA22',...
        'updatedOptic');
    
    handles.alfxPlot=modelLinkedPlotReadable(handles.opticAxes,...
        handles.myModelObject,...
        'S',...
        'ALFA11',...
        'updatedOptic');
    handles.alfyPlot=modelLinkedPlotReadable(handles.opticAxes,...
        handles.myModelObject,...
        'S',...
        'ALFA22',...
        'updatedOptic');
    
    handles.dxPlot=modelLinkedPlotReadable(handles.opticAxes,...
        handles.myModelObject,...
        'S',...
        'DISP1',...
        'updatedOptic');
    handles.dyPlot=modelLinkedPlotReadable(handles.opticAxes,...
        handles.myModelObject,...
        'S',...
        'DISP3',...
        'updatedOptic');
    
    handles.muxPlot=modelLinkedPlotReadable(handles.opticAxes,...
        handles.myModelObject,...
        'S',...
        'MU1',...
        'updatedOptic');
    handles.muyPlot=modelLinkedPlotReadable(handles.opticAxes,...
        handles.myModelObject,...
        'S',...
        'MU2',...
        'updatedOptic');
else
    handles.betxPlot=modelLinkedPlotReadable(handles.opticAxes,...
        handles.myModelObject,...
        'S',...
        'BETX',...
        'updatedOptic');
    handles.betyPlot=modelLinkedPlotReadable(handles.opticAxes,...
        handles.myModelObject,...
        'S',...
        'BETY',...
        'updatedOptic');
    
    handles.alfxPlot=modelLinkedPlotReadable(handles.opticAxes,...
        handles.myModelObject,...
        'S',...
        'ALFX',...
        'updatedOptic');
    handles.alfyPlot=modelLinkedPlotReadable(handles.opticAxes,...
        handles.myModelObject,...
        'S',...
        'ALFY',...
        'updatedOptic');
    
    handles.dxPlot=modelLinkedPlotReadable(handles.opticAxes,...
        handles.myModelObject,...
        'S',...
        'DX',...
        'updatedOptic');
    handles.dyPlot=modelLinkedPlotReadable(handles.opticAxes,...
        handles.myModelObject,...
        'S',...
        'DY',...
        'updatedOptic');
    
    handles.muxPlot=modelLinkedPlotReadable(handles.opticAxes,...
        handles.myModelObject,...
        'S',...
        'MUX',...
        'updatedOptic');
    handles.muyPlot=modelLinkedPlotReadable(handles.opticAxes,...
        handles.myModelObject,...
        'S',...
        'MUY',...
        'updatedOptic');
end

set(handles.betxPlot.handle,'Color','r','LineStyle','-','LineWidth',2);
set(handles.betxPlot.handle,'Visible','off');
set(handles.betxPlot.handle,'uicontextmenu',handles.axesMenu);

set(handles.betyPlot.handle,'Color','r','LineStyle','--','LineWidth',2);
set(handles.betyPlot.handle,'Visible','off');
set(handles.betyPlot.handle,'uicontextmenu',handles.axesMenu);
%
set(handles.alfxPlot.handle,'Color','b','LineStyle','-','LineWidth',2);
set(handles.alfxPlot.handle,'Visible','off');
set(handles.alfxPlot.handle,'uicontextmenu',handles.axesMenu);

set(handles.alfyPlot.handle,'Color','b','LineStyle','--','LineWidth',2);
set(handles.alfyPlot.handle,'Visible','off');
set(handles.alfyPlot.handle,'uicontextmenu',handles.axesMenu);
%
set(handles.dxPlot.handle,'Color',[0,0.5,0],'LineStyle','-','LineWidth',2);
set(handles.dxPlot.handle,'Visible','off');
set(handles.dxPlot.handle,'uicontextmenu',handles.axesMenu);

set(handles.dyPlot.handle,'Color',[0,0.5,0],'LineStyle','--','LineWidth',2);
set(handles.dyPlot.handle,'Visible','off');
set(handles.dyPlot.handle,'uicontextmenu',handles.axesMenu);
%
set(handles.muxPlot.handle,'Color',[0,0.5,0.5],'LineStyle','-','LineWidth',2);
set(handles.muxPlot.handle,'Visible','off');
set(handles.muxPlot.handle,'uicontextmenu',handles.axesMenu);

set(handles.muyPlot.handle,'Color',[0,0.5,0.5],'LineStyle','--','LineWidth',2);
set(handles.muyPlot.handle,'Visible','off');
set(handles.muyPlot.handle,'uicontextmenu',handles.axesMenu);
%
handles.xPlot=modelLinkedPlotReadable(handles.opticAxes,...
    handles.myModelObject,...
    'S',...
    'X',...
    'updatedOptic');
set(handles.xPlot.handle,'Color',[0.5,0.5,0.5],'LineStyle','-','LineWidth',2);
set(handles.xPlot.handle,'Visible','off');
set(handles.xPlot.handle,'uicontextmenu',handles.axesMenu);
handles.yPlot=modelLinkedPlotReadable(handles.opticAxes,...
    handles.myModelObject,...
    'S',...
    'Y',...
    'updatedOptic');
set(handles.yPlot.handle,'Color',[0.5,0.5,0.5],'LineStyle','--','LineWidth',2);
set(handles.yPlot.handle,'Visible','off');
set(handles.yPlot.handle,'uicontextmenu',handles.axesMenu);
%
handles.sigmaxPlot=modelLinkedPlotReadable(handles.opticAxes,...
    handles.myModelObject,...
    'S',...
    'lastBeamSizeX_mm',...
    'updatedOptic');
set(handles.sigmaxPlot.handle,'Color',[0.5,0.2,0.9],'LineStyle','-','LineWidth',2);
set(handles.sigmaxPlot.handle,'Visible','off');
set(handles.sigmaxPlot.handle,'uicontextmenu',handles.axesMenu);
handles.sigmayPlot=modelLinkedPlotReadable(handles.opticAxes,...
    handles.myModelObject,...
    'S',...
    'lastBeamSizeY_mm',...
    'updatedOptic');
set(handles.sigmayPlot.handle,'Color',[0.5,0.2,0.9],'LineStyle','--','LineWidth',2);
set(handles.sigmayPlot.handle,'Visible','off');
set(handles.sigmayPlot.handle,'uicontextmenu',handles.axesMenu);

%
hold(handles.opticAxes, 'off');

% plot elements of the line (WARNING: THIS IS A WEAK POINT OF THIS GUI)
axes(handles.elementsAxes)
madx2matlab.plotMachineTwissElements(handles.myModelObject.lastMachineOptic);
ylim(handles.elementsAxes,[-2,2])

% synchronize the two axes on S
linkaxes([handles.elementsAxes, handles.opticAxes],'x')

% set max xlimits
aux = [handles.myModelObject.lastMachineOptic.DATA.S];
xlim(handles.elementsAxes,[min(aux),max(aux)]);

% some axis labels legend
xlabel(handles.elementsAxes,'S [m]','FontSize',14,'FontWeight','bold');
set(handles.opticAxes,'XTickLabel',[],'FontSize',12);
set(handles.elementsAxes,'YTick',[],'FontSize',12);
%
legend(handles.opticAxes,...
    'Bet_x','Bet_y',...
    'Alf_x','Alf_y',...
    'D_x','D_y',...
    'Mu_x','Mu_y',...
    'x','y',...
    '\sigmax [mm]','\sigmay [mm]')
%
grid(handles.opticAxes);




% --- Outputs from this function are returned to the command line.
function varargout = onlineModelGUI_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in popupLocations.
function popupLocations_Callback(hObject, eventdata, handles)
% hObject    handle to popupLocations (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupLocations contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupLocations
contents = cellstr(get(hObject,'String'));
selectedItem = contents{get(hObject,'Value')};

if strcmpi(selectedItem,'Line start')
    try
        set(handles.editBetx0,'String',num2str(handles.myModelObject.lastMachineOptic.DATA(1).BETX));
        set(handles.editAlfx0,'String',num2str(handles.myModelObject.lastMachineOptic.DATA(1).ALFX));
        set(handles.editBety0,'String',num2str(handles.myModelObject.lastMachineOptic.DATA(1).BETY));
        set(handles.editAlfy0,'String',num2str(handles.myModelObject.lastMachineOptic.DATA(1).ALFY));
    catch e
        disp(['Something wrong in getting the current Twiss conditions at initial location.'])
    end
else
    try
        aux = madx2matlab.extractOpticFieldsValues(handles.myModelObject.lastMachineOptic, ...
            {selectedItem}, {'BETX', 'ALFX', 'BETY', 'ALFY'});
        
        set(handles.editBetx0,'String',num2str(aux(1)));
        set(handles.editAlfx0,'String',num2str(aux(2)));
        set(handles.editBety0,'String',num2str(aux(3)));
        set(handles.editAlfy0,'String',num2str(aux(4)));
    catch e
        disp(['Something wrong in getting the current Twiss conditions at this location: ',selectedItem])
    end
end








% --- Executes during object creation, after setting all properties.
function popupLocations_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupLocations (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editBetx0_Callback(hObject, eventdata, handles)
% hObject    handle to editBetx0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editBetx0 as text
%        str2double(get(hObject,'String')) returns contents of editBetx0 as a double


% --- Executes during object creation, after setting all properties.
function editBetx0_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editBetx0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in updateButton.
function updateButton_Callback(hObject, eventdata, handles)
% hObject    handle to updateButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.myModelObject.timerFunction('','');


% --- Executes on button press in autoUpdateButton.
function autoUpdateButton_Callback(hObject, eventdata, handles)
% hObject    handle to autoUpdateButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if get(hObject,'Value')
    handles.myModelObject.startTimer();
else
    handles.myModelObject.stopTimer();
end


function editAlfx0_Callback(hObject, eventdata, handles)
% hObject    handle to editAlfx0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editAlfx0 as text
%        str2double(get(hObject,'String')) returns contents of editAlfx0 as a double


% --- Executes during object creation, after setting all properties.
function editAlfx0_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editAlfx0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editBety0_Callback(hObject, eventdata, handles)
% hObject    handle to editBety0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editBety0 as text
%        str2double(get(hObject,'String')) returns contents of editBety0 as a double


% --- Executes during object creation, after setting all properties.
function editBety0_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editBety0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editAlfy0_Callback(hObject, eventdata, handles)
% hObject    handle to editAlfy0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editAlfy0 as text
%        str2double(get(hObject,'String')) returns contents of editAlfy0 as a double


% --- Executes during object creation, after setting all properties.
function editAlfy0_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editAlfy0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editUpdatePeriod_Callback(hObject, eventdata, handles)
% hObject    handle to editUpdatePeriod (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editUpdatePeriod as text
%        str2double(get(hObject,'String')) returns contents of editUpdatePeriod as a double
try
    handles.myModelObject.setTimerPeriod(str2double(get(hObject,'String')));
catch exception
    disp(['Unable to update timer period: ',exception.message]);
end


% --- Executes during object creation, after setting all properties.
function editUpdatePeriod_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editUpdatePeriod (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in showBetx.
function showBetx_Callback(hObject, eventdata, handles)
% hObject    handle to showBetx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of showBetx
if (get(hObject,'Value'))
    set(hObject,'BackgroundColor',[0 0.7 0]);
    set(handles.betxPlot.handle,'Visible','on')
else
    set(hObject,'BackgroundColor',[0.7 0.7 0.7]);
    set(handles.betxPlot.handle,'Visible','off')
end

% --- Executes on button press in showBety.
function showBety_Callback(hObject, eventdata, handles)
% hObject    handle to showBety (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of showBety
if (get(hObject,'Value'))
    set(hObject,'BackgroundColor',[0 0.7 0]);
    set(handles.betyPlot.handle,'Visible','on')
else
    set(hObject,'BackgroundColor',[0.7 0.7 0.7]);
    set(handles.betyPlot.handle,'Visible','off')
end

% --- Executes on button press in showAlfx.
function showAlfx_Callback(hObject, eventdata, handles)
% hObject    handle to showAlfx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of showAlfx
if (get(hObject,'Value'))
    set(hObject,'BackgroundColor',[0 0.7 0]);
    set(handles.alfxPlot.handle,'Visible','on')
else
    set(hObject,'BackgroundColor',[0.7 0.7 0.7]);
    set(handles.alfxPlot.handle,'Visible','off')
end

% --- Executes on button press in showAlfy.
function showAlfy_Callback(hObject, eventdata, handles)
% hObject    handle to showAlfy (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of showAlfy
if (get(hObject,'Value'))
    set(handles.alfyPlot.handle,'Visible','on')
    set(hObject,'BackgroundColor',[0 0.7 0]);
else
    set(hObject,'BackgroundColor',[0.7 0.7 0.7]);
    set(handles.alfyPlot.handle,'Visible','off')
end

% --- Executes on button press in showDx.
function showDx_Callback(hObject, eventdata, handles)
% hObject    handle to showDx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of showDx
if (get(hObject,'Value'))
    set(handles.dxPlot.handle,'Visible','on')
    set(hObject,'BackgroundColor',[0 0.7 0]);
else
    set(hObject,'BackgroundColor',[0.7 0.7 0.7]);
    set(handles.dxPlot.handle,'Visible','off')
end

% --- Executes on button press in showDy.
function showDy_Callback(hObject, eventdata, handles)
% hObject    handle to showDy (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of showDy
if (get(hObject,'Value'))
    set(handles.dyPlot.handle,'Visible','on')
    set(hObject,'BackgroundColor',[0 0.7 0]);
else
    set(hObject,'BackgroundColor',[0.7 0.7 0.7]);
    set(handles.dyPlot.handle,'Visible','off')
end

% --- Executes on button press in showMux.
function showMux_Callback(hObject, eventdata, handles)
% hObject    handle to showMux (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of showMux
if (get(hObject,'Value'))
    set(handles.muxPlot.handle,'Visible','on')
    set(hObject,'BackgroundColor',[0 0.7 0]);
else
    set(hObject,'BackgroundColor',[0.7 0.7 0.7]);
    set(handles.muxPlot.handle,'Visible','off')
end

% --- Executes on button press in showMuy.
function showMuy_Callback(hObject, eventdata, handles)
% hObject    handle to showMuy (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of showMuy
if (get(hObject,'Value'))
    set(handles.muyPlot.handle,'Visible','on')
    set(hObject,'BackgroundColor',[0 0.7 0]);
else
    set(hObject,'BackgroundColor',[0.7 0.7 0.7]);
    set(handles.muyPlot.handle,'Visible','off')
end




% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% delete reference to different plots
try
    handles.betxPlot.delete();
    handles.betyPlot.delete();
    handles.alfxPlot.delete();
    handles.alfyPlot.delete();
    handles.dxPlot.delete();
    handles.dyPlot.delete();
    handles.muxPlot.delete();
    handles.muyPlot.delete();
    handles.xPlot.delete();
    handles.yPlot.delete();
    handles.sigmaxPlot.delete();
    handles.sigmayPlot.delete();
    
    % local listeners
    delete(handles.myListenerComputed);
    delete(handles.myListenerComputing);
    delete(handles.myListenerCompatationError);
catch e
    disp(['Some error deleting some plot...',...
        'I will go ahead, but you will get some troubles',...
        'maybe with your matlab instance...sorry. ',e.message])
end

% Hint: delete(hObject) closes the figure
delete(hObject);


% --- Executes on button press in cumputeInitialButton.
function cumputeInitialButton_Callback(hObject, eventdata, handles)
% hObject    handle to cumputeInitialButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
possibleLocations = cellstr(get(handles.popupLocations,'String'));
selectedLocation = possibleLocations{get(handles.popupLocations,'Value')};
try
    handles.myModelObject.initialDELTAP = ...
        str2num(get(handles.editDeltaP,'String'));
    handles.myModelObject.nominalGeoEmittanceX_mmmrad = ...
        str2num(get(handles.editEmitX,'String'));
    handles.myModelObject.nominalGeoEmittanceY_mmmrad = ...
        str2num(get(handles.editEmitY,'String'));
    handles.myModelObject.nominalEnergySpread = ...
        str2num(get(handles.editESpread,'String'));
    
    handles.myModelObject.backTraceInitialConditions(...
        selectedLocation,...
        str2double(get(handles.editBetx0,'String')),...
        str2double(get(handles.editAlfx0,'String')),...
        str2double(get(handles.editBety0,'String')),...
        str2double(get(handles.editAlfy0,'String')));
catch exception
    disp(['onlineModelGUI::cumputeInitialButton_Callback: ',...
        'Unable to back trace initial conditions: ',exception.getReport]);
end


% --- Executes on button press in considerCorrectorsButton.
function considerCorrectorsButton_Callback(hObject, eventdata, handles)
% hObject    handle to considerCorrectorsButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of considerCorrectorsButton
if (get(hObject,'Value'))
    handles.myModelObject.considerCorrectors = true;
else
    handles.myModelObject.considerCorrectors = false;
end



function editDeltaP_Callback(hObject, eventdata, handles)
% hObject    handle to editDeltaP (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editDeltaP as text
%        str2double(get(hObject,'String')) returns contents of editDeltaP as a double


% --- Executes during object creation, after setting all properties.
function editDeltaP_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editDeltaP (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on mouse press over axes background.
function opticAxes_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to opticAxes (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

myParent = get(hObject,'parent');
myType = get(myParent,'SelectionType');
if strcmpi(myType,'alt')
    auxS = get(hObject,'CurrentPoint');
    auxS = auxS(1,1);
    
    try
        auxModelS = [handles.myModelObject.lastMachineOptic.DATA.S];
    catch e
        error(['Unable to get S data from the model:', e.message]);
    end
    
    % all the information I need are of this index
    [~, auxIdx] = min(abs(auxModelS-auxS));
    
    % set the proper labels
    set(handles.axesMenuItemName,'Label',handles.myModelObject.lastMachineOptic.DATA(auxIdx).NAME);
    if handles.usePTCtwiss
        set(handles.axesMenuItemBetx,'Label',['Betx = ', num2str(handles.myModelObject.lastMachineOptic.DATA(auxIdx).BETA11)]);
        set(handles.axesMenuItemBety,'Label',['Bety = ', num2str(handles.myModelObject.lastMachineOptic.DATA(auxIdx).BETA22)]);
        set(handles.axesMenuItemAlfx,'Label',['Alfx = ', num2str(handles.myModelObject.lastMachineOptic.DATA(auxIdx).ALFA11)]);
        set(handles.axesMenuItemAlfy,'Label',['Alfy = ', num2str(handles.myModelObject.lastMachineOptic.DATA(auxIdx).ALFA22)]);
        set(handles.axesMenuItemDx,'Label',['Dx = ', num2str(handles.myModelObject.lastMachineOptic.DATA(auxIdx).DISP1)]);
        set(handles.axesMenuItemDy,'Label',['Dy = ', num2str(handles.myModelObject.lastMachineOptic.DATA(auxIdx).DISP3)]);
        set(handles.axesMenuItemMux,'Label',['Mux = ', num2str(handles.myModelObject.lastMachineOptic.DATA(auxIdx).MU1)]);
        set(handles.axesMenuItemMuy,'Label',['Muy = ', num2str(handles.myModelObject.lastMachineOptic.DATA(auxIdx).MU2)]);
    else
        set(handles.axesMenuItemBetx,'Label',['Betx = ', num2str(handles.myModelObject.lastMachineOptic.DATA(auxIdx).BETX)]);
        set(handles.axesMenuItemBety,'Label',['Bety = ', num2str(handles.myModelObject.lastMachineOptic.DATA(auxIdx).BETY)]);
        set(handles.axesMenuItemAlfx,'Label',['Alfx = ', num2str(handles.myModelObject.lastMachineOptic.DATA(auxIdx).ALFX)]);
        set(handles.axesMenuItemAlfy,'Label',['Alfy = ', num2str(handles.myModelObject.lastMachineOptic.DATA(auxIdx).ALFY)]);
        set(handles.axesMenuItemDx,'Label',['Dx = ', num2str(handles.myModelObject.lastMachineOptic.DATA(auxIdx).DX)]);
        set(handles.axesMenuItemDy,'Label',['Dy = ', num2str(handles.myModelObject.lastMachineOptic.DATA(auxIdx).DY)]);
        set(handles.axesMenuItemMux,'Label',['Mux = ', num2str(handles.myModelObject.lastMachineOptic.DATA(auxIdx).MUX)]);
        set(handles.axesMenuItemMuy,'Label',['Muy = ', num2str(handles.myModelObject.lastMachineOptic.DATA(auxIdx).MUY)]);
    end
    
    set(handles.axesMenuItemX,'Label',['x = ', num2str(handles.myModelObject.lastMachineOptic.DATA(auxIdx).X)]);
    set(handles.axesMenuItemY,'Label',['y = ', num2str(handles.myModelObject.lastMachineOptic.DATA(auxIdx).Y)]);
end


% --------------------------------------------------------------------
function uitogglezoomtool_OffCallback(hObject, eventdata, handles)
% hObject    handle to uitoggletool1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.opticAxes,'ButtonDownFcn',...
    @(hObject,eventdata)onlineModelGUI('opticAxes_ButtonDownFcn',hObject,eventdata,guidata(hObject)));
set(handles.elementsAxes,'ButtonDownFcn',...
    @(hObject,eventdata)onlineModelGUI('opticAxes_ButtonDownFcn',hObject,eventdata,guidata(hObject)));



% --- Executes on button press in showX.
function showX_Callback(hObject, eventdata, handles)
% hObject    handle to showX (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of showX
if (get(hObject,'Value'))
    set(handles.xPlot.handle,'Visible','on')
    set(hObject,'BackgroundColor',[0 0.7 0]);
else
    set(hObject,'BackgroundColor',[0.7 0.7 0.7]);
    set(handles.xPlot.handle,'Visible','off')
end

% --- Executes on button press in showY.
function showY_Callback(hObject, eventdata, handles)
% hObject    handle to showY (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of showY
if (get(hObject,'Value'))
    set(handles.yPlot.handle,'Visible','on')
    set(hObject,'BackgroundColor',[0 0.7 0]);
else
    set(hObject,'BackgroundColor',[0.7 0.7 0.7]);
    set(handles.yPlot.handle,'Visible','off')
end


% --- Executes on button press in modifyCurrentBtn.
function modifyCurrentBtn_Callback(hObject, eventdata, handles)
% hObject    handle to modifyCurrentBtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% open a table GUI
tableGUI(handles.myModelObject);

function opticComputationDone(handles)
set(handles.busyLabel, 'String','Ready','BackgroundColor',[0,1,0]);
pause(0.1);

function opticComputationStarted(handles)
set(handles.busyLabel, 'String','Computing optic...','BackgroundColor',[1,0,0]);
pause(0.1);

function opticComputationError(handles)
set(handles.busyLabel, 'String','Error occured...','BackgroundColor',[0,1,1]);
pause(1);


% --- Executes on button press in showSigmaX.
function showSigmaX_Callback(hObject, eventdata, handles)
% hObject    handle to showSigmaX (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of showY
if (get(hObject,'Value'))
    set(handles.sigmaxPlot.handle,'Visible','on')
    set(hObject,'BackgroundColor',[0 0.7 0]);
else
    set(hObject,'BackgroundColor',[0.7 0.7 0.7]);
    set(handles.sigmaxPlot.handle,'Visible','off')
end

% --- Executes on button press in showSigmaY.
function showSigmaY_Callback(hObject, eventdata, handles)
% hObject    handle to showSigmaY (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of showY
if (get(hObject,'Value'))
    set(handles.sigmayPlot.handle,'Visible','on')
    set(hObject,'BackgroundColor',[0 0.7 0]);
else
    set(hObject,'BackgroundColor',[0.7 0.7 0.7]);
    set(handles.sigmayPlot.handle,'Visible','off')
end


function editESpread_Callback(hObject, eventdata, handles)
% hObject    handle to editESpread (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editESpread as text
%        str2double(get(hObject,'String')) returns contents of editESpread as a double


% --- Executes during object creation, after setting all properties.
function editESpread_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editESpread (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editEmitX_Callback(hObject, eventdata, handles)
% hObject    handle to editEmitX (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editEmitX as text
%        str2double(get(hObject,'String')) returns contents of editEmitX as a double


% --- Executes during object creation, after setting all properties.
function editEmitX_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editEmitX (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editEmitY_Callback(hObject, eventdata, handles)
% hObject    handle to editEmitY (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editEmitY as text
%        str2double(get(hObject,'String')) returns contents of editEmitY as a double


% --- Executes during object creation, after setting all properties.
function editEmitY_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editEmitY (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
