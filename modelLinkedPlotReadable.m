classdef modelLinkedPlotReadable < handle
    %
    % Handles of a polygonal line
    %
    % Davide Gamba - Guido Sterbini
    % feb 2014
    
    properties
        myLink
        myLinkY
        myLinkX
        handle
        myListener
    end
    
    methods
        % constructor
        function obj=modelLinkedPlotReadable(myAxes,onlineModelObject,opticsDataX,opticsDataY,myEventForRedrawing)
            y=[onlineModelObject.lastMachineOptic.DATA.(opticsDataY)];
            x=[onlineModelObject.lastMachineOptic.DATA.(opticsDataX)];
            
            if isempty(y)
                obj.handle=plot([NaN],'Parent',myAxes);
            elseif numel(x) ~= numel(y)
                disp(['Something wrong with x,y dimensions of x=',opticsDataX,' y=',opticsDataY,...
                    ]);
                obj.handle=plot([NaN],'Parent',myAxes);
            else
                obj.handle=plot(x', y','Parent',myAxes);
            end
            obj.myLink=onlineModelObject;
            
            obj.myLinkY=opticsDataY;
            obj.myLinkX=opticsDataX;
            
            obj.myListener=addlistener(onlineModelObject,myEventForRedrawing,@obj.redraw);
        end
        
        function redraw(obj,h,e)
            auxX = [ obj.myLink.lastMachineOptic.DATA.(obj.myLinkX) ];
            auxY = [ obj.myLink.lastMachineOptic.DATA.(obj.myLinkY) ];
            try
                if ishandle(obj.handle)
                    set(obj.handle,'YData',auxY,'XData',auxX);
                else
                    warning(['Looks like I lost the handle to my axes for ',obj.myLinkY])
                    disp('Removing listener...');
                    delete(obj.myListener);
                end
            catch exc
                error(['linkedPlotXYReadable::Impossible to redraw: ',exc.message])
            end
        end
        
        function delete(obj)
            delete(obj.myListener);
        end
        
    end
end
