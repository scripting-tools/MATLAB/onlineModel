
classdef onlineModel < handle
    %ONLINEMODEL Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        % initial line conditions
        initialBETX = 1;
        initialBETY = 1;
        initialALFX = 0;
        initialALFY = 0;
        %
        initialDELTAP = 0;
        %
        initialX = 0;
        initialY = 0;
        initialPX = 0;
        initialPY = 0;
        %
        initialDX = 0;
        initialDPX = 0;
        initialDY = 0;
        initialDPY = 0;
        
        % the model
        machineModel
        % the last computed optics
        lastMachineOptic
        
        % variables used to compute nominal beam Size along the machine
        nominalGeoEmittanceX_mmmrad = 1;
        nominalGeoEmittanceY_mmmrad = 1;
        nominalEnergySpread = 0.01;
        lastBeamSizeX_mm = [];
        lastBeamSizeY_mm = [];
        
        % define if the corrector values has to be
        considerCorrectors = false;
        
        correctorsCurrents = {};
        quadrupoleCurrents = {};
        otherCurrents = {};
        
        sequenceName = '';
        backSequenceName = '';
    end
    
    properties (Access=private)
        myTimer
    end
    
    
    methods
        function obj=onlineModel(madxScriptFile, madxParameterFile, sequenceName, ...
                backSequenceName, madxExecutableFile)

            
            % create model
            obj.machineModel = madx2matlab(madxScriptFile,...
                madxParameterFile,...
                madxExecutableFile);
            obj.sequenceName = sequenceName;
            obj.backSequenceName = backSequenceName;
            obj.machineModel.twissCommandLine={
                ['use, period=',obj.sequenceName,';'],...
                [...
                'select, flag=twiss, clear;',char(10),...
                'select, flag=twiss, column=name, KEYWORD, s, x, y, px, py, l, k1l, k2l,', char(10),...
                'betx,bety,alfx,alfy,',char(10),...
                'dx,dy,mux,muy,',char(10),...
                'RE56,T;',char(10),...
                'TWISS, RMATRIX,',char(10),...
                'BETX=initialBETX,ALFX=initialALFX,',char(10),...
                'BETY=initialBETY,ALFY=initialALFY,',char(10),...
                'DX=initialDX,DPX=initialDPX,DY=initialDY,DPY=initialDPY,',char(10),...
                'X=initialX,PX=initialPX,Y=initialY,PY=initialPY,',char(10),...
                'DELTAP=initialDELTAP;',char(10),...
                ]};
            
            % compute initial optic
            %obj.updateMagnets();
            %obj.updateOptics();
            
            % create update timer
            auxCallback = @obj.timerFunction;
            obj.myTimer = timer(...
                'ExecutionMode','fixedRate',...
                'Period', 10, ...
                'TimerFcn', auxCallback);
        end
        
        function startTimer(obj)
            start(obj.myTimer);
        end
        
        function stopTimer(obj)
            stop(obj.myTimer);
        end
        
        function setTimerPeriod(obj, period)
            if period < 30
                disp('Setting a too short period! I will overwrite it to 30 seconds.')
                period = 30;
            end
            
            if strcmp(get(obj.myTimer,'Running'), 'on')
                stop(obj.myTimer);
                set(obj.myTimer,'Period',period);
                start(obj.myTimer);
            else
                set(obj.myTimer,'Period',period);
            end
        end
        
        function period = getTimerPeriod(obj)
            period = get(obj.myTimer,'Period');
        end
        
        function myBoolean = isTimerRunning(obj)
            if strcmp(obj.myTimer.Running, 'off')
                myBoolean = false;
            else
                myBoolean = true;
            end
        end
        
        function timerFunction(obj, ~, ~)
            obj.updateMagnets();
            obj.updateOptics();
        end
        
        function delete(obj)
            % be sure to delete the timer related to this GUI.
            delete(obj.myTimer);
        end
        
        function updateMagnets(obj)
            % get magnet parameters
            obj.machineModel.setModelParameters(...
                obj.quadrupoleCurrents,...
                obj.machineModel.getMachineParameters(obj.quadrupoleCurrents));
            %
            if obj.considerCorrectors
                obj.machineModel.setModelParameters(...
                    obj.correctorsCurrents,...
                    obj.machineModel.getMachineParameters(obj.correctorsCurrents));
            else
                obj.machineModel.setModelParameters(...
                    obj.correctorsCurrents,...
                    zeros(numel(obj.correctorsCurrents),1));
            end
                        
            % just to slow it down a bit.
            pause(0.5);
        end
        
        function updateOptics(obj)
            % notify
            notify(obj, 'computingOptic');
            
            % set general beam parameters
            obj.machineModel.setModelParameters('initialBETX', obj.initialBETX);
            obj.machineModel.setModelParameters('initialBETY', obj.initialBETY);
            obj.machineModel.setModelParameters('initialALFX', obj.initialALFX);
            obj.machineModel.setModelParameters('initialALFY', obj.initialALFY);
            obj.machineModel.setModelParameters('initialDELTAP', obj.initialDELTAP);
            obj.machineModel.setModelParameters('initialX', obj.initialX);
            obj.machineModel.setModelParameters('initialY', obj.initialY);
            obj.machineModel.setModelParameters('initialPX', obj.initialPX);
            obj.machineModel.setModelParameters('initialPY', obj.initialPY);
            obj.machineModel.setModelParameters('initialDX', obj.initialDX);
            obj.machineModel.setModelParameters('initialDY', obj.initialDY);
            obj.machineModel.setModelParameters('initialDPX', obj.initialDPX);
            obj.machineModel.setModelParameters('initialDPY', obj.initialDPY);
            
            % compute optics
            try
                obj.lastMachineOptic = obj.machineModel.computeOptics();
                % compute also the nominal beam sizes
                obj.computeBeamSizes();
            catch e
                disp(['onlineModel::error computing machine optic:',e.message]);
                notify(obj, 'errorComputingOptic');
            end
            
            % notify events
            notify(obj, 'updatedOptic');
        end
        function backTraceInitialConditions(obj, from, betx, alfx, bety, alfy)

            switch upper(from)
                case 'LINE START'
                    % that is easy. Just set them and return.
                    obj.initialBETX = betx;
                    obj.initialBETY = bety;
                    obj.initialALFX = alfx;
                    obj.initialALFY = alfy;
                    % trigger an update optic
                    obj.updateOptics();
                    %%%%%%%%%%%%%%%%%%%%%%
                    return;
                    %%%%%%%%%%%%%%%%%%%%%%
                otherwise
                    if ~isempty(obj.backSequenceName)
                        auxuseline = ['use, period=',obj.backSequenceName,', range=',from,'/#e;',char(10)];
                    else
                        auxuseline = [...
                            'seqedit, sequence=',obj.sequenceName,', range=',from,'/#e;',char(10),...
                            'flatten;', char(10), 'reflect;', char(10),...
                            'use, period=',obj.sequenceName,';',char(10)];
                    end
            end
            
            % prepare a special twiss code to execute
            auxCommand = [...
                'initialBETX = ',num2str(betx),';',char(10),...
                'initialBETY = ',num2str(bety),';',char(10),...
                'initialALFX = ',num2str(-alfx),';',char(10),...
                'initialALFY = ',num2str(-alfy),';',char(10),...
                auxuseline,...
                obj.machineModel.twissCommandLine{2},char(10)];
            
            % set some anyway common settings.
            obj.machineModel.setModelParameters('initialDELTAP', obj.initialDELTAP);
            
            % notify
            notify(obj, 'computingOptic');
            
            % execute this personalized code
            auxMadResultText = obj.machineModel.generalExecMadxScript(auxCommand, obj.machineModel.twissGetOutputCommandLine);
            auxOpticsTable = madx2matlab.parseTFSTable(auxMadResultText);
            
            % here are my new settings
            obj.initialBETX = auxOpticsTable.DATA(end).BETX;
            obj.initialBETY = auxOpticsTable.DATA(end).BETY;
            obj.initialALFX = - auxOpticsTable.DATA(end).ALFX;
            obj.initialALFY = - auxOpticsTable.DATA(end).ALFY;
            
            % trigger an update optic
            obj.updateOptics();
        end
        
        function computeBeamSizes(obj)
            
            for i=1:length(obj.lastMachineOptic.DATA)
                try
                    auxEBetaX = obj.nominalGeoEmittanceX_mmmrad.*obj.lastMachineOptic.DATA(i).BETX;
                    auxEBetaY = obj.nominalGeoEmittanceY_mmmrad.*obj.lastMachineOptic.DATA(i).BETY;
                    auxDPDX = obj.nominalEnergySpread.*obj.lastMachineOptic.DATA(i).DX;
                    auxDPDY = obj.nominalEnergySpread.*obj.lastMachineOptic.DATA(i).DY;
                catch e1
                    try
                        % maybe I am using PTC.. retry
                        auxEBetaX = obj.nominalGeoEmittanceX_mmmrad.*obj.lastMachineOptic.DATA(i).BETA11;
                        auxEBetaY = obj.nominalGeoEmittanceY_mmmrad.*obj.lastMachineOptic.DATA(i).BETA22;
                        auxDPDX = obj.nominalEnergySpread.*obj.lastMachineOptic.DATA(i).DISP1;
                        auxDPDY = obj.nominalEnergySpread.*obj.lastMachineOptic.DATA(i).DISP3;
                    catch e2
                        auxEBetaX = NaN;
                        auxEBetaY = NaN;
                        auxDPDX = NaN;
                        auxDPDY = NaN;
                    end
                end
                
                obj.lastMachineOptic.DATA(i).lastBeamSizeX_mm = sqrt(auxEBetaX*10^-6 + auxDPDX.^2)*1000;
                obj.lastMachineOptic.DATA(i).lastBeamSizeY_mm = sqrt(auxEBetaY*10^-6 + auxDPDY.^2)*1000;
            end
            
            obj.lastBeamSizeX_mm = [obj.lastMachineOptic.DATA.lastBeamSizeX_mm];
            obj.lastBeamSizeY_mm = [obj.lastMachineOptic.DATA.lastBeamSizeY_mm];
        end
    end
    events
        computingOptic
        errorComputingOptic
        updatedOptic
    end
end

